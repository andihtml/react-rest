import React, {useEffect, useState} from 'react';
import {Link, useLocation} from "react-router-dom";
import {Descriptions} from 'antd';

const DetailPage = () => {

    let query = new URLSearchParams(useLocation().search);
    let category = query.get("category")
    let movie_id = query.get("movie_id")

    const [item, setItem] = useState(null);

    useEffect(() => {
        fetchItem();
    }, [])

    const fetchItem = async () => {
        if (movie_id) {
            const data = await fetch(('https://api.themoviedb.org/3/movie/{movie_id}?api_key=59e6636eac4c0f34a227549b9ead8fcf&language=en-US').replaceAll('{movie_id}', movie_id));
            const item = await data.json();
            console.log(item);

            if(item.poster_path != "") {
                item.poster_path = ('http://image.tmdb.org/t/p/w185{path}').replaceAll('{path}', item.poster_path);
            } else {
                item.poster_path = 'https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png'
            }

            setItem(item);
        }
    }

    return (
        <div align={"left"}>
            <Link to={('/result?category={category}').replaceAll('{category}', category)}>Back to list</Link>
            <h1 align={"center"}>Detail Page</h1>
            {(item != null) ? (
                <Descriptions title={item.title} bordered column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }} >
                    <Descriptions.Item label="poster"><img src={item.poster_path} style={{'height':"100px"}}/></Descriptions.Item>
                    <Descriptions.Item label="tagline">{item.tagline}</Descriptions.Item>
                    <Descriptions.Item label="status">{item.status}</Descriptions.Item>
                    <Descriptions.Item label="popularity">{item.popularity}</Descriptions.Item>
                    <Descriptions.Item label="spoken_languages">{item.spoken_languages[0].name}</Descriptions.Item>
                    <Descriptions.Item label="vote_average">{item.vote_average}</Descriptions.Item>
                    <Descriptions.Item label="release_date">{item.release_date}</Descriptions.Item>
                    <Descriptions.Item label="production_countries">{item.production_countries[0].name}</Descriptions.Item>
                    <Descriptions.Item label="overview">{item.overview}</Descriptions.Item>
                </Descriptions>
            ) : ""}
        </div>
    );
};

export default DetailPage;
