import HomePage from './HomePage';
import ResultPage from './ResultPage';
import DetailPage from './DetailPage';

export { HomePage, ResultPage, DetailPage };