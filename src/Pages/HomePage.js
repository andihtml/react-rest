import React from 'react';
import SearchForm from "../Components/SearchForm";

const HomePage = () => {
    return (
        <div>
            <h1>Form Page</h1>
            <SearchForm/>
        </div>
    );
};

export default HomePage;
