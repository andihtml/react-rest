import React,{useEffect, useState} from 'react';
import { useHistory, useLocation} from "react-router-dom";
import { Table } from 'antd';

const ResultPage = () => {

    const history = useHistory();
    let query = new URLSearchParams(useLocation().search);
    let category = query.get("category")

    const [items, setItems] = useState([]);

    useEffect(() => {
        fetchItems();
    },[])

    const fetchItems = async() => {
        const data = await fetch(('https://api.themoviedb.org/3/movie/{category}?api_key=59e6636eac4c0f34a227549b9ead8fcf&language=en-US&page=1').replaceAll('{category}', category));
        const items = await data.json();

        console.log(items);
        setItems(items);
    }

    const columns = [
        {
            title: 'Title (' + category+')',
            dataIndex: 'title',
            key: 'title',
            render: (text, record) => <a onClick={() => {history.push(('/detail?category={category}&movie_id={movie_id}').replaceAll('{category}',category).replaceAll('{movie_id}',record.id))}}>{text}</a>,
        },
        {
            title: 'Vote Rate',
            dataIndex: 'vote_average',
            key: 'vote_average',
        },
        {
            title: 'Release Date',
            dataIndex: 'release_date',
            key: 'release_date',
        },
    ];

    return (
        <div>
            <h1>Result Page</h1>
            <Table dataSource={items.results} columns={columns} />;
        </div>
    );
};

export default ResultPage;
