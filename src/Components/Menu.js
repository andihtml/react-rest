import React from 'react';
import {Menu} from "antd";
import { useHistory } from "react-router-dom";
import {UserOutlined, VideoCameraOutlined} from "@ant-design/icons";

const MyComponent = () => {
    const history = useHistory();

    const handleClick = e => {

        switch (e.key) {
            case "search": {
                history.push("/");
                break;
            }
            case "result": {
                history.push("/result");
                break;
            }
            case "detail": {
                history.push("/detail");
                break;
            }
            default:
                history.push("/notfound");
        }
    }

    return (
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']} onClick={handleClick}>
            <Menu.Item key="search" icon={<UserOutlined/>} >
                Home
            </Menu.Item>
        </Menu>
    );
};

export default MyComponent;
