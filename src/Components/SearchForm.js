import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import {Form, Input, Button, Checkbox, Select} from 'antd';

const {Option} = Select;

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

const SearchForm = () => {
    const history = useHistory();

    const [form] = Form.useForm();
    const [formData, setFormData] = useState({
        category: "popular"
    });

    const onFinish = values => {
        setFormData(values)
        history.push(('/result?category={category}').replaceAll('{category}', values.category));
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            {...layout}
            name="basic"
            form={form}
            layout='horizontal'
            initialValues={formData}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            <Form.Item name="category" label="Category" rules={[{required: true}]}>
                <Select
                    placeholder="Select a option and change input text above"
                    allowClear
                >
                    <Option value="popular">Popular Movie</Option>
                    <Option value="top_rated">Top Rated Movie</Option>
                    <Option value="now_playing">Now Playing Movie</Option>
                </Select>
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>

            <div>
                title: {formData.title}
            </div>
            <div>
                category: {formData.category}
            </div>

        </Form>
    );
};

export default SearchForm;
