import React from 'react';
import { useHistory } from "react-router-dom";
import {Layout, Menu, Tag} from 'antd';
import {
    AppstoreOutlined,
    BarChartOutlined,
    CloudOutlined,
    ShopOutlined,
    TeamOutlined,
    UserOutlined,
    UploadOutlined,
    VideoCameraOutlined,
} from '@ant-design/icons';
import './layout_sider.css';

const {Header, Content, Footer, Sider} = Layout;

function LayoutSider(props) {

    const history = useHistory();

    const handleClick = e => {
        switch (e.key) {
            case "search": {
                history.push("/home");
            }
        }
        console.log('click', e);
    }

    return (
        <Layout>
            <Sider
                style={{
                    overflow: 'auto',
                    height: '100vh',
                    position: 'fixed',
                    left: 0,
                }}
            >
                <div className="logo"><Tag color="red">~~ MY REACT.JS EXAM ~~</Tag></div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']} onClick={handleClick}>
                    <Menu.Item key="search" icon={<UserOutlined/>}>
                        Home
                    </Menu.Item>
                    <Menu.Item key="result" icon={<VideoCameraOutlined/>}>
                        Search Result
                    </Menu.Item>
                    <Menu.Item key="detail" icon={<VideoCameraOutlined/>}>
                        Detail
                    </Menu.Item>
                </Menu>
            </Sider>
            <Layout className="site-layout" style={{marginLeft: 200}}>
                <Header className="site-layout-background" style={{padding: 0}}/>
                <Content style={{margin: '24px 16px 0', overflow: 'initial'}}>
                    <div className="site-layout-background" style={{padding: 24, textAlign: 'center'}}>
                        {props.children}
                    </div>
                </Content>
                <Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
        </Layout>
    )
}

export default LayoutSider;
