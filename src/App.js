import './App.css';
import './index.css';
import 'antd/dist/antd.css';
import './Layout/layout_sider.css';
import React from "react";
import {BrowserRouter as Router, Switch, Route, useHistory} from "react-router-dom";
import {Layout, Tag} from 'antd';

import Menu from "./Components/Menu";
import {HomePage, ResultPage, DetailPage} from "./Pages";

const {Header, Content, Footer, Sider} = Layout;

function App(props) {

    return (
        <Router>
            <Layout>
                <Sider
                    style={{
                        overflow: 'auto',
                        height: '100vh',
                        position: 'fixed',
                        left: 0,
                    }}
                >
                    <div className="logo"><Tag color="red">~~ MY REACT.JS EXAM ~~</Tag></div>
                    <Menu/>
                </Sider>
                <Layout className="site-layout" style={{marginLeft: 200}}>
                    <Header className="site-layout-background" style={{padding: 0}}/>
                    <Content style={{margin: '24px 16px 0', overflow: 'initial'}}>
                        <div className="site-layout-background" style={{padding: 24, textAlign: 'center'}}>

                            <Switch>
                                <Route exact path={"/"} component={HomePage}/>
                                <Route exact path={"/result"} component={ResultPage}/>
                                <Route exact path={"/detail"} component={DetailPage}/>
                                <Route render={() => <h1>Not found!</h1>} />
                            </Switch>

                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>
                </Layout>
            </Layout>
        </Router>
    );
}

export default App;
